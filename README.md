# README #

Programming Challenge 2

Design an efficient program that prints out, in reverse order, every multiple of 7 that is between 1 and 300. Extend the program to other multiples and number ranges. Write the program in any programming language of your choice.


* programmingChallengePart1 contains code for every multiple of 7 that is between 1 and 300.

* programmingChallenge2 Extend the program to other multiples and number ranges.